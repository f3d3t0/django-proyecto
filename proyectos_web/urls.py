from django.contrib import admin
from django.urls import path
from django.urls import include
from proyectos_templates.views import VistaPrincipal

app_name = "proyectos_templates"

urlpatterns = [
    path('', include('proyectos_templates.urls')),
]
