import django_tables2 as tables

from django.views.generic import CreateView, TemplateView
from django.views.generic.list import ListView
from django.views.generic.edit import FormView
from django.urls import reverse_lazy

from .models import Entrada, Proyecto
from .forms import ProyectoForm, EntradaForm
from .tables import ProyectoTable, EntradaTable

class CrearEntradaFormView(CreateView):
    model = Entrada
    form_class = EntradaForm
    template_name = 'crear_entrada.html'
    success_url = reverse_lazy('proyecto:vista_principal')


class CrearProyectoFormView(CreateView):
    model = Proyecto
    form_class = ProyectoForm
    template_name = 'crear_proyecto.html'
    success_url = reverse_lazy('proyecto:vista_principal')



class VistaPrincipal(tables.SingleTableView):
    model = Proyecto
    template_name = 'principal.html'
    table_class = ProyectoTable

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['entradas'] = EntradaTable(Entrada.objects.all())
        return context
