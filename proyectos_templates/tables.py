import django_tables2 as tables
from .models import Proyecto, Entrada


class EntradaTable(tables.Table):
    class Meta:
        model = Entrada
        fields = ['texto']


class ProyectoTable(tables.Table):
    class Meta:
        model = Proyecto
        fields = ['autor', 'nombre', 'fecha_creacion']
