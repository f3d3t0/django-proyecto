from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit

from django import forms
from django.urls import reverse_lazy

from .models import Entrada, Proyecto

class EntradaForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Cargar'))

    class Meta:
        model = Entrada
        fields = '__all__'
        widgets = {
            'texto': forms.TextInput(attrs={'placeholder': 'Ingrese su entrada aquí'}),
        }


class ProyectoForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.add_input(Submit('submit', 'Cargar'))

    class Meta:
        model = Proyecto
        fields = ['autor', 'nombre','fecha_creacion']
        widgets = {
            'autor': forms.TextInput(attrs={'placeholder': 'Ingrese el nombre del proyecto aquí'}),
            'nombre': forms.TextInput(attrs={'placeholder': 'Ingrese el nombre del autor aquí'}),
            'fecha_creacion': forms.DateTimeInput(attrs={'type':'datetime-local'})
        }
