from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import path
from .views import VistaPrincipal,CrearEntradaFormView, CrearProyectoFormView

app_name = 'proyecto'

urlpatterns = [
    path('', VistaPrincipal.as_view(), name='vista_principal'),
    path('crear-entrada/', CrearEntradaFormView.as_view(), name='crear_entrada'),
    path('crear-proyecto/', CrearProyectoFormView.as_view(), name='crear_proyecto'),
]

urlpatterns += staticfiles_urlpatterns()