from django.apps import AppConfig


class ProyectosTemplatesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'proyectos_templates'
