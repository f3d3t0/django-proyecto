from django.db import models


class Proyecto(models.Model):
    nombre = models.CharField(max_length=500)
    autor = models.CharField(max_length=500)
    fecha_creacion = models.DateTimeField()

    def __str__(self):
        return self.nombre

class Entrada(models.Model):
    texto = models.CharField(max_length=500)

    def __str__(self):
        return self.texto
