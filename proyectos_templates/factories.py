import factory
from .models import Proyecto, Entrada

class ProyectoFactory(factory.Factory):
    class Meta:
        model = Proyecto
    nombre = factory.Faker('first_name')
    autor = factory.Faker('first_name')
    fecha_creacion = factory.Faker('datetime')


class EntradaFactory(factory.Factory):
    class Meta:
        model = Entrada

    texto = factory.Faker('bs')
