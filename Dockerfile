# Usa una imagen base de Python
FROM python:3.10-slim-bullseye

ENV PYTHONDONTWRITEBYTECODE=abc

USER root
WORKDIR /app
COPY . .

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
    libpq-dev \
    build-essential

RUN /usr/local/bin/python -m pip install --upgrade pip
# Copia los requerimientos a la imagen y los instala
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt

# Establecer la variable de entorno de Django
ENV DJANGO_SETTINGS_MODULE=proyectos_web.settings

# Correr los comandos necesarios para inicializar el proyecto
RUN python manage.py collectstatic --noinput
# RUN python manage.py migrate

# Iniciar la aplicación
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "proyectos_web.wsgi", "--reload"]
